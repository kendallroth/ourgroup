# Contributing Guidelines

> Thanks for contributing to the project!

This document contains an overview of the technology stack used in this project; for more detailed explanations/instructions see the dedicated `README` files within each main directory.

## Prerequisites

Docker manages container dependencies, eliminating most requirements. However, there are still several mandatory development prerequisites. Docker and Docker Compose are required to manage the containers.

> **Note:** Node and NPM are only required globally **if** wanting to use the Vue CLI on the host machine.

- [Docker](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Installation

- Install all dependencies listed above
- Validate dev environment variables in root `.env`
- Build and start the Docker containers (`docker-compose up`)
  - _Requires creating database volume first (see below)_
- Run migrations (_inside Django container_)
- Create Django superuser for admin panels (_inside Django container_)
- _(OPTIONAL)_: Enable VS Code "Remote Extensions" for better dev experience

### Notes

Due to a [Windows permissions bug](https://github.com/cytopia/devilbox/issues/175) when mounting the database container storage to a local folder (like API code is mounted), Docker volumes must be used instead. This is recommended in the documentation, with the only downside being that they are not externally accessible.

The Docker volume _must_ be created **before** running the stack for the first time with `docker volume create --name ourgroup-db-data --driver local`.

## Commands

Docker Compose provides several helpful commands for interacting with containers.

> **NOTE:** Commands will eventually be wrapped with shell script...

```bash
# NOTE: Substitute CONTAINER_NAME with the appropriate container:
#   - ourgroup-api (Django API)
#   - ourgroup-app (Vue app)
#   - ourgroup-db  (PostgreSQL database)

# Start the Docker containers (or single container)
docker-compose up [CONTAINER_NAME]

# Rebuild docker containers (or single container)
#   NOTE: Only necessary when container configuration has changed
docker-compose build [CONTAINER_NAME]

# List running containers
docker-compose ps

# Show logs from running container
docker-compose logs -f CONTAINER_NAME

# Open a shell in a container
docker-compose exec CONTAINER_NAME bash|sh
# Windows may need to prefix with "winpty" for TTY access
winpty docker-compose exec [CONTAINER_NAME] bash

# Shut down all containers
docker-compose down
```

## Environment Variables

Ensure that a `.env` file exists in the project with default settings (included in repository).

> **NOTE:** The main `.env` must be symbolically linked into both the `/app` and `/api` directories; this is especially hard to create on Windows. However, it is already added to the repository and should not require updating the symbolic links.

## API

The API is a basic Python app using the Django framework (coupled with Django REST for API resource structure).

- [Python 3](https://www.python.org/) - High-level scripting language
- [Django](https://www.djangoproject.com/) - High-level Python Web framework that encourages rapid development
- [Django Graphene](https://graphene-python.org/) - GraphQL framework for Django web app

Django provides an admin interface for managing the database models, called Django Admin. All models should have a custom Admin interface configuration to simplify development. A Django "superuser" must be created **inside** the container with `python manage.py createsuperuser` to use the admin panel (accessed at `localhost:[PORT]/admin`). Django is also used for creating, inspecting, and applying database migrations (see Database guide below).

New packages can be installed from **within** the container with `pipenv install [PACKAGE]`. `pipenv` handles installing the `pip` packages **within** the proper virtual environment (permissions, etc). After packages have been added the Docker container **must** be rebuilt. The `pipenv` shell can be accessed **within** the container with `pipenv shell`, granting access to the `django-admin` administration CLI.

### Notes

Logging into the Django Admin will create a session cookie, effectively keeping the same user signed into the Vue app!

_TBD_

## Database

The database is a PostgreSQL instance, but is primarily managed via the Django CLI (migrations, etc).

- [PostgreSQL](https://postgresql.org) - Open source RDBMS with a reputation for reliability and performance

Django provides a database migration system via the Django CLI, useful for creating and applying database migrations. Migrations can be generated from **within** the container with `python manage.py makemigrations` and applied with `python manage.py migrate`.

##  App

The app is a basic Vue app using the Vue CLI and accompanying ecosystem of Vue packages (Vuex, Vue Router, etc).

- [Vue](https://vuejs.org) - Progressive JS framework built on standard HTML and CSS skills
- [Vue CLI](https://cli.vuejs.org) - Standard tooling and configuration wrappers for VueJS development
- [Vuetify](https://vuetifyjs.com) - Vue UI library with beautiful Material Design components

The Vue CLI provides a simply wrapper around the project configuration (Webpack, CLI plugins, etc). It is accessible from the `vue.config.js` configuration file **or** through a new UI interface, started by running `vue ui` (if Vue is installed globally).

New NPM packages can be installed from **within** the container with `npm install [PACKAGE]`, which is where `node_modules` resides (not locally).

_TDB_

## VS Code

VS Code offers an extension to make working on Docker Containers easier, enabling a development experience more suited to the environment itself. Since the `node_modules` are installed in the Docker container, the local VS Code cannot access them, leading to issues with ES Lint, etc. However, the [Remote Containers guide](https://code.visualstudio.com/docs/remote/containers#_attaching-to-running-containers) offers a quick way to connect to a running container (likely on the app) and installing remote extensions or even opening a remote terminal through VS Code.

> **NOTE:** Only a single remote container can be attached to a VS Code window, meaning that multiple windows would be necessary to connect to all project (if wanted).

- Start Docker containers (must be running)
- Select "Attach to Running Container" from VS Remote Explorer view (_may need to install extension_)
  - _Select a workspace folder the first time_
- _(Optional)_: Customize container configuration (`devcontainer.json`)
- _(Optional)_: Extensions can be installed remotely
- _(Optional)_: Terminal can be opened into container through VS Code (if extension is installed)


# ourgroup Database

> Database is managed entirely by Docker Compose, and the data is stored in a volume (managed by Docker).

## Development

- TBD

## Migrations

#### Data Migrations

Managing data migrations becomes complicated when rolling up other "regular" migrations into a single file(s). It may be necessary to temporarily move the data migrations out of the `migrations` folder until `makemigrations` has been run again, and then update the data migration `dependencies` as appropriate.

## Standards

There are several standards and naming conventions for database development (via Django models).

- Use `snake_case` for all model fields (and database columns)
- Append `_at` to dates representing an "auditable" event, but `_date` to dates representing a "regular" event
  - `created_at` and `verified_at` represent an event in time, while `start_date` represents a regular event

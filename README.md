# ourgroup

Simple replacement for Google Groups, offering a top-down approach to managing a group's communication.

## Development

Read the [Contributing](CONTRIBUTING.md) document for developer instructions (setup, etc.).


 Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Basic settings page (change password workflow) (!4)
- Basic profile page (!3)
- Authentication workflow (!2)
- Initial project setup (!1)

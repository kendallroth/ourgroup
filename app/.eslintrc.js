module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ["plugin:vue/essential", "eslint:recommended", "@vue/prettier"],
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "warn",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "warn",
    "no-unused-vars": "warn",
    "no-tabs": "warn",
    "no-trailing-spaces": "warn",
    "no-unreachable": "warn",
    "prefer-const": "warn",
    "prefer-destructuring": "warn",
    quotes: ["warn", "double"],
    semi: ["warn", "always"],
    "vue/html-self-closing": "off",
    "vue/no-unused-components": "warn",
    "vue/no-unused-vars": "warn",
  },
  parserOptions: {
    parser: "babel-eslint",
  },
};

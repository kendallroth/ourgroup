module.exports = {
  configureWebpack: {
    resolve: {
      alias: {
        "@components": "/app/src/components",
        "@config": "/app/src/config",
        "@graphql": "/app/src/graphql",
        "@plugins": "/app/src/plugins",
        "@store": "/app/src/store",
        "@styles": "/app/src/styles",
        "@utilities": "/app/src/utilities",
        "@views": "/app/src/views",
      },
      extensions: [".gql"],
    },
  },
  devServer: {
    // Webpack proxy is used in devlopment to eliminate CORS by proxying requests
    //   through the API itself (using Docker network).
    proxy: {
      "^/graphql/": {
        target: "http://ourgroup-api:5000/",
        changeOrigin: true,
      },
    },
    watchOptions: {
      poll: true,
    },
  },
  transpileDependencies: ["vuetify"],
};

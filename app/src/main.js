import Vue from "vue";
import VueApollo from "vue-apollo";
import { Plugin as FragmentPlugin } from "vue-fragment";
import VueRouter from "vue-router";
import { sync as syncRouterVuex } from "vuex-router-sync";
import VueShortKey from "vue-shortkey";

// Components
import App from "./App";

// Utilities
import "./registerServiceWorker";
import router from "./router.js";
import store from "./store";
import apolloProvider from "./vue-apollo";

// Plugins
import {
  ComponentsPlugin,
  ErrorsPlugin,
  SnackbarPlugin,
  VeeValidate,
  Vuetify,
} from "@plugins";

// Styles
import "@styles/app.scss";
import "@styles/backgrounds.scss";
import "@styles/vuetify.scss";

Vue.config.productionTip = false;

// Register plugins
Vue.use(ComponentsPlugin);
Vue.use(ErrorsPlugin);
Vue.use(FragmentPlugin);
Vue.use(SnackbarPlugin);
Vue.use(VeeValidate);
Vue.use(Vuetify);
Vue.use(VueRouter);
Vue.use(VueApollo);
Vue.use(VueShortKey);
// NOTE: Vuex must be registered in "store" file

// Register Vue Router with Vuex for better tracking routes in Vuex
syncRouterVuex(store, router);

new Vue({
  router,
  store,
  apolloProvider,
  vuetify: Vuetify,
  render: (h) => h(App),
}).$mount("#app");

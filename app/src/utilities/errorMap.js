export default {
  // NOTE: Requesting invalid PostgreSQL UUID raises an exception
  INVALID_UUID: "ID is invalid",
  NOT_AUTHORIZED: "User is not authenticated",
  NOT_VERIFIED: "User is not verified",
  // NOTE: Validation errors should always (all) be properly displayed in form UI
  VALIDATION_ERRORS: "Please fix the errors",

  USER_ACTIVATION_RESEND__ALREADY_ACTIVATED: "Account is already activated",

  USER_ACTIVATE__CODE_INVALID: "Invalid activation code",
  USER_ACTIVATE__CODE_EXPIRE: "Expired activation code",
  USER_ACTIVATE__CODE_INVALIDATED: "Invalidated activation code",
  USER_ACTIVATE__CODE_USED_ALREADY: "Already used activation code",

  USER_PASSWORD_CHANGE__INVALID_CREDENTIALS: "Invalid current password",
  USER_PASSWORD_CHANGE__PASSWORD_INVALID: "New password is invalid",
  USER_PASSWORD_CHANGE__PASSWORD_SAME: "New password is same as previous",
  USER_PASSWORD_CHANGE__PASSWORD_WEAK: "New password is too weak",

  USER_PASSWORD_RESET__CODE_INVALID: "Invalid password reset code",
  USER_PASSWORD_RESET__CODE_EXPIRED: "Expired password reset code",
  USER_PASSWORD_RESET__CODE_USED_ALREADY: "Already used password reset code",
  USER_PASSWORD_RESET__CODE_INVALIDATED: "Invalid password reset code",
  USER_PASSWORD_RESET__PASSWORD_INVALID: "New password is invalid",
  USER_PASSWORD_RESET__PASSWORD_SAME: "New password is same as previous",
  USER_PASSWORD_RESET__PASSWORD_WEAK: "New password is too weak",

  USER_SIGNIN__INACTIVE_USER: "User is inactive",
  USER_SIGNIN__INVALID_CREDENTIALS: "Invalid credentials",
  USER_SIGNIN__UNKNOWN_ERROR: "Failed to sign in",
  USER_SIGNIN__UNVERIFIED_USER: "User is unactivated",

  USER_SIGNUP__DUPLICATE_ACCOUNT: "Email is already in use",
  USER_SIGNUP__INVALID_NAME: "Name is invalid",
  USER_SIGNUP__INVALID_PASSWORD: "Password is invalid",
  USER_SIGNUP__PASSWORD_INVALID: "Password is invalid",
  USER_SIGNUP__PASSWORD_WEAK: "Password is too weak",
};

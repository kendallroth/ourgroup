import axios from "axios";
import qs from "qs";

// Utilities
import config from "@config";

const api = axios.create({
  baseUrl: config.api.url,
  timeout: 10000,
  // TODO: Change eventually
  withCredentials: false,
  // Parameters are serialized to support arrays (bracket/repeated notation).
  // NOTE: This doesn't work with arrays of objects, which require "index" mode.
  //         However, since this is a slightly longer query is should be
  //         only used where appropriate (by overriding in request).
  paramsSerializer: (params) =>
    qs.stringify(params, { arrayFormat: "brackets" }),
});

export default api;

/**
 * Interface for local storage
 */

// Utilities
import { MEDIA_QUERY_VISIBILITY_KEY } from "@utilities/constants";

/**
 * Wrapper around local storage for known entries in the app
 */
class Storage {
  /**
   * Get a boolean value from local storage
   * @param  {string}  key - Local storage key
   * @return {boolean}     - Boolean value
   */
  static getBoolean(key) {
    const booleanValue = localStorage.getItem(key);

    try {
      return JSON.parse(booleanValue);
    } catch (e) {
      return false;
    }
  }

  /**
   * Determine whether media query indicator is visible
   * @return {boolean} - Whether the media query indicator is visible
   */
  static getMediaQueryVisibility() {
    return this.getBoolean(MEDIA_QUERY_VISIBILITY_KEY);
  }

  /**
   * Toggle whether the media query indicator is visible (primarily for development)
   * @param {boolean} isVisible - Whether the indicator is visible
   */
  static setMediaQueryVisibility(isVisible) {
    localStorage.setItem(MEDIA_QUERY_VISIBILITY_KEY, isVisible);
  }
}

export default Storage;

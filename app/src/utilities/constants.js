// Need to export all constants as named exports to easily import in components

// Authentication token
// export const JWT_KEY = "auth-token";
// Local storage logout key (for syncing logout across tabs)
export const LOGOUT_KEY = "logout";
// Whether media query indicator is visible
export const MEDIA_QUERY_VISIBILITY_KEY = "media-query-visible";

// Utilities
import config from "@config";
import { apolloClient as Apollo } from "@/vue-apollo";
import Store from "@store";
import {
  AUTH__AUTH_SET,
  AUTH__AUTH_REMOVE,
  AUTH__REFRESH_TIMEOUT_REMOVE,
  AUTH__REFRESH_TIMEOUT_SET,
} from "@store/types";

// GraphQL
import DeleteTokenCookieMutation from "@graphql/mutations/Auth/DeleteTokenCookie";
import DeleteRefreshTokenCookieMutation from "@graphql/mutations/Auth/DeleteRefreshTokenCookie";
import RefreshTokenMutation from "@graphql/mutations/Auth/RefreshToken";
import RevokeTokenMutation from "@graphql/mutations/Auth/RevokeToken";

class Auth {
  /**
   * Initialize the JWt authentication (starts JWT refresh cycle)
   */
  static init() {
    window.addEventListener("beforeunload", function () {
      Store.commit(`auth/${AUTH__REFRESH_TIMEOUT_REMOVE}`);
    });

    // Refresh JWT when loading app and set up automatic refresh cycles
    //   to prevent token expiry (although it would be handled by Apollo).
    this.refresh().catch(() => {
      // NOTE: Empty catch statement prevents error from propagating!
    });
  }

  /**
   * Determine whether user is flagged as authenticated
   *
   * Since JWT and refresh tokens are stored in cookies, this is the only
   *   indicator the app has about authentication status.
   *
   * @return {Object} Authenticated user
   */
  static getAuth() {
    return Boolean(Store.state.auth.user);
  }

  /**
   * Remove the authenticated user
   */
  static removeAuth() {
    Store.commit(`auth/${AUTH__AUTH_REMOVE}`);

    // Remove the automatic refresh token calls
    Store.commit(`auth/${AUTH__REFRESH_TIMEOUT_REMOVE}`);
  }

  /**
   * Set the authenticated user
   * @param {Object} user - Authenticated user
   */
  static setAuth(user) {
    Store.commit(`auth/${AUTH__AUTH_SET}`, user);
  }

  /**
   * Refresh the current JWT (and trigger automatic refresh)
   */
  static async refresh() {
    const response = await Apollo.mutate({ mutation: RefreshTokenMutation });

    if (!config.app.isProduction) {
      // eslint-disable-next-line no-console
      console.info("JWT token was refreshed");
    }

    const { payload } = response.data.refreshToken;
    const { exp } = payload;

    const expiryTime = exp;
    const currentTime = Date.now().valueOf() / 1000;
    const timeToExpiry = Math.floor(expiryTime - currentTime);

    // Set a timeout to automatically refresh the token before this one expires
    Store.dispatch(`auth/${AUTH__REFRESH_TIMEOUT_SET}`, timeToExpiry);
  }

  /**
   * Store the authenticated user
   * @param {Object}   user - Authenticated user
   * @param {function} cb   - Callback
   */
  static signin(user, cb = null) {
    this.setAuth(user);

    cb && cb();
  }

  /**
   * Signout the user
   *
   * Since JWT tokens (stored in cookies) are used for authentication,
   *   it is necessary to remove the JWT and refresh tokens.
   *
   * @param {function} cb - Callback
   */
  static async signout(cb = null) {
    this.removeAuth();

    // Remove the automatic refresh token calls
    Store.commit(`auth/${AUTH__REFRESH_TIMEOUT_REMOVE}`);

    // NOTE: Operations are performed separately and in order of importance,
    //         in case one of them somehow fails.

    // Revoke the refresh token
    try {
      await Apollo.mutate({ mutation: RevokeTokenMutation });
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error("Error revoking refresh token. Please signout again!");
    }

    // Delete the JWT cookie and refresh token
    try {
      await Apollo.mutate({ mutation: DeleteRefreshTokenCookieMutation });
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error("Error deleting refresh token. Please signout again!");
    }

    try {
      await Apollo.mutate({ mutation: DeleteTokenCookieMutation });
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error("Error deleting auth token. Please signout again!");
    }

    // Optional callback (router redirection, etc)
    cb && cb();

    return true;
  }
}

export default Auth;

// Utilities
import { version } from "../package.json";

export default {
  api: {
    url: process.env.VUE_APP_API_URL,
  },
  app: {
    envName: process.env.VUE_APP_ENV_NAME || "local",
    isProduction: process.env.NODE_ENV === "production",
    version,
  },
};

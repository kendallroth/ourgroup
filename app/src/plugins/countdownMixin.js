const DEFAULT_COUNTDOWN_LENGTH = 30;
const MILLISECONDS_IN_SECOND = 1000;

/**
 * Configure and manage a countdown timer
 *
 * Can be used for preventing re-sending emails quickly, etc.
 */
const CountdownMixin = {
  data() {
    return {
      countdown: 0,
      $countdownIntervalRef: null,
    };
  },
  computed: {
    /**
     * Whether countdown is active
     */
    isCountdownActive() {
      return this.countdown > 0;
    },
  },
  /**
   * Cleanup countdown intervals
   */
  beforeDestroy() {
    clearInterval(this.$countdownIntervalRef);
  },
  methods: {
    /**
     * Configure and start the countdown
     * @param {number} seconds - Length of countdown
     */
    startCountdown(seconds = DEFAULT_COUNTDOWN_LENGTH) {
      clearInterval(this.$countdownIntervalRef);
      this.countdown = seconds;

      // Configure timeout before user can resend the email
      this.$countdownIntervalRef = setInterval(() => {
        this.countdown--;

        if (this.countdown <= 0) {
          this.countdown = 0;
          clearInterval(this.$countdownIntervalRef);
        }
      }, MILLISECONDS_IN_SECOND);
    },
  },
};

export default CountdownMixin;

// Utilities
import errorMap from "@utilities/errorMap";

const DEFAULT_ERROR = "An error occurred";

/**
 * Global error handling plugin
 */
const ErrorsPlugin = {
  /**
   * Plugin installation function
   * @param {Object} Vue     - Vue object
   * @param {Object} options - Plugin options
   */
  install: (Vue) => {
    Vue.prototype.$getError = getError;
    Vue.prototype.$getFormErrors = getFormErrors;
    Vue.prototype.$hasError = hasError;
  },
};

/**
 * Get the corresponding error message to an API error response code
 * @param  {Object|string} error          - Error object or code
 * @param  {string}        defaultMessage - Message to use if no matching error code is found
 * @return {string}                       - Error message corresponding to error (code)
 */
const getError = (error, defaultMessage = DEFAULT_ERROR) => {
  if (!error) return defaultMessage;

  let errorCode = null;

  // GraphQL and other errors are exposed as objects
  if (typeof error === "object") {
    const { graphQLErrors, message } = error;

    // NOTE: Must check "message" last as Apollo will set it to a non-error code,
    //         which should not be used if "graphQLErrors" is present.
    // NOTE: This does not handle the "extensions" key used for validation errors,
    //         as the default code will always be "VALIDATION_ERRORS" in that case.
    if (graphQLErrors && graphQLErrors.length > 0) {
      errorCode = graphQLErrors[0].message;
    } else if (message) {
      errorCode = message;
    }
  }
  // The bare error code may be provided instead of an error object
  else if (typeof error === "string") {
    errorCode = error;
  }

  return errorMap[errorCode] || defaultMessage;
};

/**
 * Get a list of form errors
 *
 * @example
 * errorChecks = [{ field: "email", code: "WRONG_EMAIL" }]
 * errorChecks = [{ field: "email", code: "WRONG_EMAIL", message: "Custom message" }]
 *
 * @param  {Object[]} errorChecks - Error checks (fields, codes, messages)
 * @param  {Object}   errors      - GraphQL response errors
 * @return {Object}               - Form errors (by field)
 */
const getFormErrors = (errorChecks, errors) => {
  return errorChecks.reduce((accum, check) => {
    if (!hasError(errors, check.code)) return accum;

    const existingFieldErrors = accum[check.field] || [];

    // Error message will be taken by default from error map (unless specified)
    const errorMessage = check.message || getError(check.code, "Invalid data");
    if (!errorMessage) return accum;

    return {
      ...accum,
      [check.field]: [...existingFieldErrors, errorMessage],
    };
  }, {});
};

/**
 * Whether an error includes a specific error response
 * @param  {Object|string}  error     - Error object
 * @param  {string}         errorCode - Error code being compared against error
 * @return {boolean}                  - Whether the error includes the error code
 */
const hasError = (error, errorCode) => {
  if (!error) return false;

  if (typeof error === "string") {
    return error === errorCode;
  }

  if (Array.isArray(error)) {
    return error.some((e) =>
      typeof e === "object" ? e.message === errorCode : e === errorCode
    );
  }

  if (typeof error === "object") {
    const { graphQLErrors = [], message } = error;

    const hasGraphQLError = graphQLErrors.some((gqlError) => {
      const { extensions, message } = gqlError;

      // Validation errors are stored in the error "extension" property,
      //   and should be checked before the error code
      return extensions && extensions.errors && extensions.errors.length > 0
        ? extensions.errors.some((extError) => extError === errorCode)
        : message === errorCode;
    });

    if (hasGraphQLError) return true;
    if (errorCode === message) return true;
  }

  // Should always be false, but hey...
  return error === errorCode;
};

export { hasError };
export default ErrorsPlugin;

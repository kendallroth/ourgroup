import { extend, setInteractionMode } from "vee-validate";
import {
  confirmed,
  email,
  max,
  min,
  numeric,
  required,
} from "vee-validate/dist/rules";

/**
 * Validation rules for vee-validate
 */
const VeeValidatePlugin = {
  /**
   * Plugin installation function
   * @param {Object} Vue     - Vue object
   * @param {Object} options - Plugin options
   */
  install: () => {
    // Only validate on blur OR every change if invalid
    setInteractionMode("eager");

    // Name part check (two parts, with optional length)
    extend("name", {
      params: ["min"],
      validate: (value, args) => {
        // Split the name (by spaces) and remove empty parts
        const nameParts = value.split(" ").filter((part) => Boolean(part));

        // Ensure name has enough parts (first and last)
        if (nameParts.length < 2) {
          return "{_field_} must include first and last name";
        }

        let nameLength = 0;

        // Ensure each name part is "long enough"
        nameParts.forEach((part) => {
          if (part.length < 2) {
            return "{_field_} has invalid parts";
          }

          nameLength += part.length;
        });

        // Ensure name ovall is "long enough"
        if (args.min && nameLength < args.min) {
          return "{_field_} must be at least {min}";
        }

        return true;
      },
    });
    // Password confirmation check
    extend("password", {
      params: ["target"],
      validate(value, { target }) {
        return value === target;
      },
      message: "Password confirmation does not match",
    });
    extend("not_password", {
      params: ["target"],
      validate(value, { target }) {
        return value !== target;
      },
      message: "Password must not match previous",
    });
    // Positive check
    extend("positive", (value) => value >= 0);

    // VeeValidate validation rules
    extend("confirmed", {
      ...confirmed,
      message: "{_field_} must match {target}",
    });
    extend("email", email);
    extend("max", {
      ...max,
      params: ["length"],
      message: "{_field_} cannot be more than {length}",
    });
    extend("min", {
      ...min,
      params: ["length"],
      message: "{_field_} must be at least {length}",
    });
    extend("numeric", {
      ...numeric,
      message: "{_field_} must be only numbers",
    });
    extend("required", {
      ...required,
      message: "{_field_} is required",
    });
  },
};

export default VeeValidatePlugin;

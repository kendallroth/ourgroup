// Utilities
import store from "@store";
import { UI__NOTIFICATION_OPEN } from "@store/types";

/**
 * Global snackbar notification plugin (wrapper for Vuex)
 */
const SnackbarPlugin = {
  /**
   * Plugin installation function
   * @param {Object} Vue     - Vue object
   * @param {Object} options - Plugin options
   */
  install: (Vue) => {
    Vue.prototype.$notify = notify;
    Vue.prototype.$notifyError = notifyError;
  },
};

/**
 * Trigger a snackbar notification
 * @param {string} message - Snackbar message
 * @param {Object} options - Snackbar configuration options
 */
const notify = (message, options) => {
  const payload = {
    ...options,
    text: message,
  };

  store.dispatch(`ui/${UI__NOTIFICATION_OPEN}`, payload);
};

/**
 * Trigger an error snackbar notification
 * @param {string} message - Snackbar error message
 * @param {Object} options - Snackbar configuration options
 */
const notifyError = (message, options) => {
  const errorOptions = { ...options, type: "error" };

  notify(message, errorOptions);
};

export default SnackbarPlugin;

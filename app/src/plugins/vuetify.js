import Vue from "vue";
import Vuetify, { VTextField } from "vuetify/lib";

Vue.use(Vuetify);

// Extend textfield component
const ExtendedTextField = {
  name: "v-text-field",
  extends: VTextField,
  mixins: [
    {
      props: {
        dense: {
          default: true,
        },
        outlined: {
          default: true,
        },
      },
    },
  ],
};

// NOTE: Extended/wrapped Vuetify components should use "vw" prefix
Vue.component("vw-text-field", ExtendedTextField);

const options = {};

export default new Vuetify(options);

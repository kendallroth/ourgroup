// Components
import * as Components from "@components/index";
import { PageLayout, TitleBar } from "@components/layout";
import * as VuetifyOverrides from "@components/vuetify";

const ComponentsPlugin = {
  /**
   * Plugin installation function
   * @param {Object} Vue     - Vue object
   * @param {Object} options - Plugin options
   */
  install: (Vue) => {
    Vue.component(PageLayout.name, PageLayout);
    Vue.component(TitleBar.name, TitleBar);

    // Register miscellaneous components
    Object.keys(Components).forEach((key) => {
      const component = Components[key];
      Vue.component(component.name, component);
    });

    // Register Vuetify components
    Object.keys(VuetifyOverrides).forEach((key) => {
      const component = VuetifyOverrides[key];
      Vue.component(component.name, component);
    });
  },
};

export default ComponentsPlugin;

// Remove user authentication flag
export const AUTH__AUTH_REMOVE = "authFlagRemove";
// Set user authentication flag
export const AUTH__AUTH_SET = "authFlagSet";
// Remove the JWT refresh timeout
export const AUTH__REFRESH_TIMEOUT_REMOVE = "authRefreshTimeoutRemove";
// Set/reset the JWT refresh timeout
export const AUTH__REFRESH_TIMEOUT_SET = "authRefreshTimeoutSet";

// Close snackbar notification manually
export const UI__NOTIFICATION_CLOSE = "notificationClose";
// Open snackbar notification (action)
export const UI__NOTIFICATION_OPEN = "notificationOpen";
// Set the new snackbar options
export const UI__NOTIFICATION_SET = "notificationSet";

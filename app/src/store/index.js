import Vue from "vue";
import Vuex, { Store } from "vuex";

// Utilities
import { auth, ui } from "@store/modules";

// Vuex must included in Vue before store can be created
Vue.use(Vuex);

export default new Store({
  modules: {
    auth,
    ui,
  },
  // NOTE: Strict mode runs a deep watcher to detect changes, not suitable for production
  strict: process.env.NODE_ENV !== "production",
});

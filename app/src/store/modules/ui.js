// Utilities
import {
  UI__NOTIFICATION_CLOSE,
  UI__NOTIFICATION_OPEN,
  UI__NOTIFICATION_SET,
} from "@store/types";

// Constants
// Delay snackbar slightly to allow previous snackbars to close (animate) properly
const SNACKBAR_DELAY = 100;

const defaultSnackbarState = {
  text: "",
  visible: false,
  onClick: null,
  onClose: null,
};

export default {
  namespaced: true,
  state: {
    // Snackbar visibility and configuration options (placehodler items only)
    snackbar: {
      ...defaultSnackbarState,
    },
  },
  mutations: {
    /**
     * Close the snackbar notification manually
     * @param {Object} state   - Vuex state
     * @param {Object} payload - Snackbar configuration options
     */
    [UI__NOTIFICATION_CLOSE](state) {
      state.snackbar = { ...defaultSnackbarState };
      state.snackbar.visible = false;
    },
    /**
     * Set the snackbar notification options
     * @param {Object} state   - Vuex state
     * @param {Object} payload - Snackbar configuration options
     */
    [UI__NOTIFICATION_SET](state, payload) {
      state.snackbar = { visible: true, ...payload };
    },
  },
  actions: {
    /**
     * Trigger a snackbar notification
     * @param {Object}   context - Store context
     * @param {function} commit  - Mutation commit handler
     * @param {string}   payload - Snackbar configuration options
     */
    [UI__NOTIFICATION_OPEN]({ commit }, payload) {
      // Hide previous notification (but don't hide other values so fade animation doesn't break)
      commit(UI__NOTIFICATION_CLOSE);

      // Display new notification (but give time for previous to disappear)
      setTimeout(() => {
        commit(UI__NOTIFICATION_SET, payload);
      }, SNACKBAR_DELAY);
    },
  },
};

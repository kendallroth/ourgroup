// Utilities
import {
  AUTH__AUTH_REMOVE,
  AUTH__AUTH_SET,
  AUTH__REFRESH_TIMEOUT_REMOVE,
  AUTH__REFRESH_TIMEOUT_SET,
} from "@store/types";
import Auth from "@utilities/auth";

export default {
  namespaced: true,
  state: {
    user: null,
    refreshTimeout: null,
  },
  getters: {
    isAuthenticated(state) {
      return Boolean(state.user);
    },
  },
  mutations: {
    /**
     * Remove the authenticated user
     * @param {Object} state - Vuex state
     */
    [AUTH__AUTH_REMOVE](state) {
      state.user = null;
    },
    /**
     * Set the authenticated user
     * @param {Object} state   - Vuex state
     * @param {Object} payload - Payload (user)
     */
    [AUTH__AUTH_SET](state, payload) {
      state.user = payload;
    },
    /**
     * Remove the auth refresh token timeout
     * @param {Object} state - Vuex state
     */
    [AUTH__REFRESH_TIMEOUT_REMOVE](state) {
      clearTimeout(state.refreshTimeout);
    },
    /**
     * Set/reset the auth refresh token timeout
     * @param {Object} state   - Vuex state
     * @param {number} payload - Refresh timeout reference
     */
    [AUTH__REFRESH_TIMEOUT_SET](state, payload) {
      state.refreshTimeout = payload;
    },
  },
  actions: {
    /**
     * Set/reset the auth refresh token timeout
     * @param {Object} store   - Vuex store
     * @param {number} payload - Token expiry (seconds)
     */
    [AUTH__REFRESH_TIMEOUT_SET]({ commit }, payload) {
      commit(AUTH__REFRESH_TIMEOUT_REMOVE);

      // Refresh a token 30 seconds before it will expire
      const preExpiryLeeway = 30;
      const timeoutDelay = (payload - preExpiryLeeway) * 1000;

      const timeout = setTimeout(() => {
        Auth.refresh();
      }, timeoutDelay);

      commit(AUTH__REFRESH_TIMEOUT_SET, timeout);
    },
  },
};

import VueRouter from "vue-router";

// Views
import Home from "@views/Home";
import ActivateAccount from "@views/Auth/ActivateAccount";
import ActivationResend from "@views/Auth/ActivationResend";
import PageNotFound from "@views/PageNotFound";
import PasswordForget from "@views/Auth/PasswordForget";
import PasswordReset from "@views/Auth/PasswordReset";
import Signin from "@views/Auth/Signin";
import Signout from "@views/Auth/Signout";
import Signup from "@views/Auth/Signup";
import User from "@views/User/User";
import UserProfile from "@views/User/Profile";
import UserSettings from "@views/User/Settings";

// Utilities
import Store from "@store";

/*
 * There are several types of route protection:
 *   - requiresAuth         - Route is unaccessible when not authenticated
 *   - requiresNoAuth       - Route is unaccessible when authenticated
 *
 * There are also a few other related properties (not route protection)
 *   - isEmailCode          - Route handles an email code/link
 *
 * There are also additional meta properties:
 *   - noScrollOnParamChange - Prevent scrolling when the route parameters changes
 */

const routes = [
  {
    path: "/signout",
    name: "signout",
    component: Signout,
    // NOTE: Should always be accessible for authentication cleanup purposes
  },
  // Unauthenticated routes
  {
    path: "/signin",
    name: "signin",
    component: Signin,
    meta: { requiresNoAuth: true },
  },
  {
    path: "/signup",
    name: "signup",
    component: Signup,
    meta: { requiresNoAuth: true },
  },
  {
    path: "/forgot-password",
    name: "forgotPassword",
    component: PasswordForget,
    meta: { requiresNoAuth: true },
  },
  {
    path: "/reset-password/:code",
    name: "resetPassword",
    component: PasswordReset,
    meta: { requiresNoAuth: true, isEmailCode: true },
  },
  {
    path: "/activate-account/:code",
    name: "activateAccount",
    component: ActivateAccount,
    meta: { requiresNoAuth: true, isEmailCode: true },
  },
  {
    path: "/resend-activation",
    name: "resendActivation",
    component: ActivationResend,
    meta: { requiresNoAuth: true },
  },
  // Authenticated routes
  {
    path: "/user",
    name: "user",
    component: User,
    meta: { requiresAuth: true },
    redirect: "/user/profile",
    children: [
      {
        path: "profile",
        name: "userProfile",
        component: UserProfile,
      },
      {
        path: "settings",
        name: "userSettings",
        component: UserSettings,
      },
    ],
  },
  // Other routes
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "*",
    name: "pageNotFound",
    component: PageNotFound,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior(to, from) {
    const isSamePage = to.name === from.name;
    const isSameBasePath = to.path === from.path;

    // Disable scrolling when the route doesn't change
    if (isSameBasePath) return false;

    // Disable scrolling on a page when both the to and from routes specify.
    //   This can be used wherever the route does change (parameter filters, etc).
    if (
      isSamePage &&
      to.matched.some((m) => m.meta && m.meta.noScrollOnParamChange) &&
      from.matched.some((m) => m.meta && m.meta.noScrollOnParamChange)
    ) {
      return false;
    }

    return { x: 0, y: 0 };
  },
  routes,
});

// Configure routing guards (primarily for authentication)
router.beforeEach((to, from, next) => {
  // Determine whether app is loading initially or routing internally
  //   NOTE: Requires all routes to have "name" set in the routing config
  const isRoutedByApp = Boolean(from.name);

  // Authentication is indicated by a Vuex user object and flag
  const isAuthenticated = Store.getters["auth/isAuthenticated"];

  // Some routes require authentication (all matching routes must be checked)
  const requiresAuth = to.matched.some((r) => r.meta && r.meta.requiresAuth);

  // Protect routes that require authentication
  if (requiresAuth) {
    if (requiresAuth && !isAuthenticated) {
      // TODO: Possibly prompt the users to login via a dialog

      // Directly accessing protected routes BEFORE authentication is determine (ie. page load)
      //   should let the authentication query handler reroute if necessary.
      // Otherwise, redirect the login page and provide a redirect back.
      return isRoutedByApp
        ? next({ path: "/signin", query: { redirectUrl: to.fullPath } })
        : next();
    }

    return next();
  } else if (to.matched.some((r) => r.meta && r.meta.requiresNoAuth)) {
    // Some routes cannot be accessed when authenticated (all matching routes must be checked)
    if (isAuthenticated) {
      // Cancel navigation if it occurred within the app (not a refresh or manual route load).
      //   Otherwise, redirect to home (necessary to avoid invalid route state).
      return isRoutedByApp ? next(false) : next("/");
    }

    return next();
  }

  next();
});

export default router;

import { InMemoryCache } from "apollo-cache-inmemory";
import { ApolloClient } from "apollo-client";
import { ApolloLink, Observable } from "apollo-link";
import { onError } from "apollo-link-error";
import { createHttpLink } from "apollo-link-http";
import VueApollo from "vue-apollo";

// Utilities
import config from "@config";
import Auth from "@utilities/auth";

// GraphQL
import RefreshTokenMutation from "@graphql/mutations/Auth/RefreshToken";

const httpLink = createHttpLink({
  // Webpack proxy is used in devlopment to eliminate CORS by proxying requests
  //   through the API itself (using Docker network).
  uri: config.app.isProduction ? config.api.url : "/graphql/",
});

/**
 * Convert a Promise to an Observable (for Apollo links)
 * Taken from: https://github.com/apollographql/apollo-link/issues/646#issuecomment-423279220
 * @param  {Promise} promise - Promise to convert
 * @return {Stream}          - Converted Observable
 */
const promiseToObservable = (promise) =>
  new Observable((subscriber) => {
    promise.then(
      (value) => {
        if (subscriber.closed) return;
        subscriber.next(value);
        subscriber.complete();
      },
      (err) => subscriber.error(err)
    );
    return subscriber; // this line can removed, as per next comment
  });

// Error link for handling GraphQL errors
const errorLink = onError(
  ({ forward, graphQLErrors, networkError, operation }) => {
    // if (response.errors && response.errors.length > 0) {
    // TODO: Possibly re-enable this in the future to force mutation callback errors.
    //         Currently, Apollo treats anything with non-null 'data' as a success.
    //         However, data is set to an object even if an error occurred.
    //  response.data = null;
    // }

    if (graphQLErrors) {
      for (const err of graphQLErrors) {
        if (err && err.message) {
          switch (err.message) {
            // Handle missing refresh tokens (when trying to refresh a JWT)
            case "Refresh token is required":
              break;
            // Retry queries/mutations after attempting to refetch a valid JWT
            case "NOT_AUTHORIZED":
              // TODO: After refreshing start countdown to next refresh (based on expiry)

              // Taken from a variety of sources:
              //   - https://www.apollographql.com/docs/link/links/error/
              //   - https://github.com/apollographql/apollo-link/issues/646#issuecomment-423279220
              return promiseToObservable(Auth.refresh()).flatMap(() =>
                forward(operation)
              );
          }
        }
      }
    }

    if (networkError) {
      // eslint-disable-next-line no-console
      console.error(`[Network error]: ${networkError}`);
    }
  }
);

// Remove __typename fields to prevent GraphQL mutation input validation errors
const omitTypenameLink = new ApolloLink((operation, forward) => {
  /* There are places in the app that we pass the results of a query directly
   *   to the component data (including the __typename values). However, when
   *   we send this object back in a mutation it is rejected by GraphQL
   *   because it does not match the input type (due to __typename).
   * Previously it was handled by deleting manually on query update, but this
   *   caused problems if forgotten or if the safety checks were forgotten on
   *   the object it was deleted from (ie. deleting from null).
   * This link strips out all __typename fields in mutations (and queries...)
   *   to prevent this from happening.
   *
   * NOTE: May be possible to only run on mutations by checking operation
   */

  // Final approach: https://github.com/apollographql/apollo-client/issues/2160
  // Taken from: https://github.com/apollographql/apollo-client/issues/1564#issuecomment-357492659
  // Another example (longer): https://gist.github.com/cdelgadob/4041818430bc5802016332dbe5611570

  const omitTypename = (key, value) =>
    key === "__typename" ? undefined : value;

  // NOTE: This has the caveat of only working for primitives (no files, etc).
  if (operation.variables && !operation.variables.file) {
    operation.variables = JSON.parse(
      JSON.stringify(operation.variables, omitTypename)
    );
  }

  return forward && forward(operation);
});

const link = ApolloLink.from([omitTypenameLink, errorLink, httpLink]);

const cache = new InMemoryCache();

const apolloClient = new ApolloClient({
  link,
  cache,
  connectToDevTools: !config.app.isProduction,
  defaultOptions: {
    query: {
      fetchPolicy: "cache-first",
    },
  },
});

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
});

export { apolloClient };
export default apolloProvider;

"""Project GraphQL schema."""

import graphene

import user.graphql.schema as user_schema


class Query(user_schema.Query, graphene.ObjectType):
    """Root query."""


class Mutation(user_schema.Mutation, graphene.ObjectType):
    """Root mutation."""


schema = graphene.Schema(query=Query, mutation=Mutation)

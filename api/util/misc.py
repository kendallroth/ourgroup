"""Miscellaneous functionality."""

from django.core.validators import RegexValidator, ValidationError
from django.urls import reverse
from django.utils.safestring import mark_safe
from graphql import GraphQLError

from util.errors import Errors

phone_regex = RegexValidator(
    regex=r"^\+?1?\d{9,15}$",
    message=(
        "Phone number must be entered in the format: '+999999999'. "
        "Up to 15 digits allowed."
    ),
)


def catch_uuid(func):
    """Catch PostgreSQL UUID errors.

    Any functions (mutations, resolvers, etc) that may throw a invalid UUID exception
    should be wrapped with this decorator to gracefully handle the error.
    """

    def func_wrapper(*args, **kwargs):
        """Wrap function."""
        try:
            return func(*args, **kwargs)
        except ValidationError as e:
            uuid_error = "is not a valid UUID"
            if uuid_error in e.message:
                raise GraphQLError(Errors.INVALID_UUID)

            raise e
        except Exception as e:
            raise e

    return func_wrapper


def get_associated_admin_link(obj, model_attr, model_path, description_attr=None):
    """Create a Django admin link to an associated object.

    Args:
        obj (obj): Django admin model object
        model_attr (str): Linked attribute name on admin model (ie. "profile" for
            "user.profile")
        model_path (str): Django app name and model name for URL (separated by
            underscore)
        description_attr (str, optional): Linked object attribute to be shown in
            link description (after ID)
    """
    # Get linked object from model
    link_obj = getattr(obj, model_attr)
    if not link_obj:
        return None

    # Get linked object ID
    link_id = getattr(link_obj, "id")
    if not link_id:
        return None

    # Internal Django admin link ("change" form displays details)
    url = reverse("admin:{}_change".format(model_path), args=[link_id])
    if not url:
        return None

    # Optional link description
    link_description = ""
    if description_attr:
        description = getattr(link_obj, description_attr)
        if description:
            link_description = " - {}".format(description)

    return mark_safe(
        "<a href='{}'>{} ({}){}</a>".format(
            url, model_attr.capitalize(), link_id, link_description
        )
    )


def get_generic_object_admin_link(obj, id_attr="owner_id", type_attr="owner_type"):
    """Create a Django admin link to an associated generic object.

    Args:
        obj (obj): Django admin model object
        id_attr (string, optional): Attribute name on object referring to
            related object ID
        type_attr (string, optional): Attribute name on object referring to
            related object Django type
    """
    # Generic foreign key uses "django_content_type" table to track the
    #   owner's type and id.
    owner_id = getattr(obj, id_attr)
    owner_type = getattr(obj, type_attr)

    # "django_content_type" table tracks owner's app and model
    owner_app = getattr(owner_type, "app_label")
    owner_model = getattr(owner_type, "model")

    if not owner_app or not owner_model:
        return None

    # Owner's app and model are used to formulate the Django admin path
    model_path = "{}_{}".format(owner_app, owner_model)
    url = reverse("admin:{}_change".format(model_path), args=[owner_id])

    return mark_safe(
        "<a href='{}'>{} ({})</a>".format(url, owner_model.capitalize(), owner_id)
    )


def get_generic_parent_admin_link(obj):
    """Create a Django admin link to the object's generic parent object.

    Args:
        obj (obj): Django admin model object
    """
    return get_generic_object_admin_link(obj, "owner_id", "owner_type")

"""Utility models."""

import uuid

from django.db import models
from django.utils import timezone


class BaseModel(models.Model):
    """Abstract model to represent creation and update times."""

    class Meta:
        """Timestamp model meta information."""

        abstract = True

    # Fields
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class BaseEmailCode(BaseModel):
    """Model representing a generic email code."""

    class Meta:
        """BaseEmailCode model meta information."""

        abstract = True

    # Fields
    code = models.UUIDField(default=uuid.uuid4, help_text="Email code", unique=True)
    type = models.CharField(
        # choices=EMAIL_CODE_CHOICES,
        help_text="Type of email code (redundant)",
        max_length=25,
    )
    expires_at = models.DateTimeField(help_text="Date the code expires")
    used_at = models.DateTimeField(
        blank=True, help_text="Date the code was used", null=True
    )
    invalidated_at = models.DateTimeField(
        blank=True, help_text="Date the code was invalidated", null=True
    )

    # Calculated fields
    def is_expired(self):
        """Whether the code is expired."""
        return timezone.now() > self.expires_at

    is_expired.boolean = True

    def is_invalidated(self):
        """Whether the code is invalidated."""
        return self.invalidated_at is not None

    is_invalidated.boolean = True

    def is_used(self):
        """Whether the code is already used."""
        return self.used_at is not None

    is_used.boolean = True

    def is_valid(self):
        """Whether the code is valid."""
        return (
            not self.is_expired() and not self.is_invalidated() and not self.is_used()
        )

    is_valid.boolean = True

"""GraphQL types for util module."""

import graphene

from graphene import relay
from graphene_django import DjangoObjectType

from util.models import BaseEmailCode


class EmailCode(DjangoObjectType):
    """Email code type."""

    is_expired = graphene.Boolean(description="Whether the code is expired")
    is_invalidated = graphene.Boolean(description="Whether the code is invalidated")
    is_used = graphene.Boolean(description="Whether the code is used")
    is_valid = graphene.Boolean(description="Whether the code is valid")

    class Meta:
        """Meta class for EmailCode."""

        model = BaseEmailCode
        interfaces = (relay.Node,)

    def resolve_is_expired(self, info, **kwargs):
        """Expired check resolver."""
        return self.is_expired()

    def resolve_is_invalidated(self, info, **kwargs):
        """Invalidated check resolver."""
        return self.is_invalidated()

    def resolve_is_used(self, info, **kwargs):
        """Already used check resolver."""
        return self.is_used()

    def resolve_is_valid(self, info, **kwargs):
        """Is valid check resolver."""
        return self.is_valid()

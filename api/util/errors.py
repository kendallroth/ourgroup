"""Error constants."""

from graphql import GraphQLError


def handle_validations(validations):
    """Raise a validation exception if any validation errors were found.

    Examples:
        Validation should be run before any model changes (to prevent errors).
        Any validation errors should be added to the list and then handled.

        >>> validations = []
        >>> validations.append(GraphQLErrors.INVALID)
        >>> handle_validations(validations)

    Args:
        validations (str[]): List of validation error codes

    Raises:
        GraphQLValidation: If validation errors exist
    """
    if len(validations) > 0:
        raise GraphQLValidation(validations)


class Errors:
    """Constant strings used to represent generic internalerrors."""

    # Searching for an invalid UUID throws an exception
    INVALID_UUID = "INVALID_UUID"
    PASSWORD_SAME = "PASSWORD_SAME"
    PASSWORD_WEAK = "PASSWORD_WEAK"


class GraphQLErrors:
    """Constant strings used to represent graphql errors to the client."""

    NOT_AUTHORIZED = "NOT_AUTHORIZED"
    NOT_VERIFIED = "NOT_VERIFIED"
    # Validation errors are returned all at once (using GraphQL "extensions")
    VALIDATION_ERRORS = "VALIDATION_ERRORS"

    USER_ACTIVATION_RESEND__ALREADY_ACTIVATED = (
        "USER_ACTIVATION_RESEND__ALREADY_ACTIVATED"
    )

    USER_ACTIVATE__CODE_INVALID = "USER_ACTIVATE__CODE_INVALID"
    USER_ACTIVATE__CODE_EXPIRE = "USER_ACTIVATE__CODE_EXPIRE"
    USER_ACTIVATE__CODE_INVALIDATED = "USER_ACTIVATE__CODE_INVALIDATED"
    USER_ACTIVATE__CODE_USED_ALREADY = "USER_ACTIVATE__CODE_USED_ALREADY"

    USER_PASSWORD_CHANGE__INVALID_CREDENTIALS = (
        "USER_PASSWORD_CHANGE__INVALID_CREDENTIALS"
    )
    USER_PASSWORD_CHANGE__PASSWORD_INVALID = "USER_PASSWORD_CHANGE__PASSWORD_INVALID"
    USER_PASSWORD_CHANGE__PASSWORD_SAME = "USER_PASSWORD_CHANGE__PASSWORD_SAME"
    USER_PASSWORD_CHANGE__PASSWORD_WEAK = "USER_PASSWORD_CHANGE__PASSWORD_WEAK"

    USER_PASSWORD_RESET__CODE_EXPIRED = "USER_PASSWORD_RESET__CODE_EXPIRED"
    USER_PASSWORD_RESET__CODE_INVALID = "USER_PASSWORD_RESET__CODE_INVALID"
    USER_PASSWORD_RESET__CODE_INVALIDATED = "USER_PASSWORD_RESET__CODE_INVALIDATED"
    USER_PASSWORD_RESET__CODE_USED_ALREADY = "USER_PASSWORD_RESET__CODE_USED_ALREADY"
    USER_PASSWORD_RESET__PASSWORD_INVALID = "USER_PASSWORD_RESET__PASSWORD_INVALID"
    USER_PASSWORD_RESET__PASSWORD_SAME = "USER_PASSWORD_RESET__PASSWORD_SAME"
    USER_PASSWORD_RESET__PASSWORD_WEAK = "USER_PASSWORD_RESET__PASSWORD_WEAK"

    USER_SIGNIN__INACTIVE_USER = "USER_SIGNIN__INACTIVE_USER"
    USER_SIGNIN__INVALID_CREDENTIALS = "USER_SIGNIN__INVALID_CREDENTIALS"
    USER_SIGNIN__UNKNOWN_ERROR = "USER_SIGNIN__UNKNOWN_ERROR"
    USER_SIGNIN__UNVERIFIED_USER = "USER_SIGNIN__UNVERIFIED_USER"

    USER_SIGNUP__DUPLICATE_ACCOUNT = "USER_SIGNUP__DUPLICATE_ACCOUNT"
    USER_SIGNUP__INVALID_NAME = "USER_SIGNUP__INVALID_NAME"
    USER_SIGNUP__PASSWORD_INVALID = "USER_SIGNUP__PASSWORD_INVALID"
    USER_SIGNUP__PASSWORD_WEAK = "USER_SIGNUP__PASSWORD_WEAK"


class GraphQLValidation(GraphQLError):
    """
    GraphQL Validation error (uses error extensions).

    Attributes:
        validations (str[]): List of validation error codes
    """

    def __init__(self, validations, **kwargs):
        """."""
        errorKwargs = {"message": GraphQLErrors.VALIDATION_ERRORS, **kwargs}

        # Only attach extensions if validation errors are present
        if validations is not None and len(validations) > 0:
            self.validations = validations
            errorKwargs["extensions"] = {"errors": validations}

        super().__init__(**errorKwargs)

"""Authentication utilities."""

from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.utils import timezone
from graphql import GraphQLError

from util.errors import GraphQLErrors


class GraphQLAuthBackend(ModelBackend):
    """Authentication backend to customize the experience when requesting JWT tokens.

    Using a custom mutation does not allow returning a refresh token!
    """

    def authenticate(self, request, username=None, password=None):
        """Allow users to log in with their email."""
        User = get_user_model()

        if username is None or password is None:
            raise GraphQLError(GraphQLErrors.USER_SIGNIN__INVALID_CREDENTIALS)

        try:
            user = User.objects.get(email=username)

            if not user.check_password(password):
                raise GraphQLError(GraphQLErrors.USER_SIGNIN__INVALID_CREDENTIALS)

            # Only active users can sign in
            if not user.is_active:
                raise GraphQLError(GraphQLErrors.USER_SIGNIN__INACTIVE_USER)

            # Only verified users can sign in
            if user.verified_at is None:
                raise GraphQLError(GraphQLErrors.USER_SIGNIN__UNVERIFIED_USER)

            user.last_login = timezone.now()
            user.save()

            return user
        except User.DoesNotExist:
            # Nonexistant users should not reveal anything to user
            raise GraphQLError(GraphQLErrors.USER_SIGNIN__INVALID_CREDENTIALS)

    def get_user(self, user_id):
        """Get a user."""
        User = get_user_model()

        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


def require_login(func):
    """Enforce user authentication for protected mutations.

    NOTE: Should only be applied to mutation functions!
    """

    def mutator(self, info, *args, **kwargs):
        """Prevent non-authed users from accessing protected mutations."""
        if info is None or info.context is None:
            raise GraphQLError(GraphQLErrors.NOT_AUTHORIZED)

        user = info.context.user
        if user is None or user.is_anonymous:
            raise GraphQLError(GraphQLErrors.NOT_AUTHORIZED)

        return func(self, info, *args, **kwargs)

    return mutator

"""Utilities module configuration."""

from django.apps import AppConfig


class UtilConfig(AppConfig):
    """Utilities module configuration."""

    name = "util"

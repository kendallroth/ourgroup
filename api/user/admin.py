"""User admin panels."""

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as UserAdminBase

# Models
from user.models import UserEmailCode, Profile, User
from util.misc import get_associated_admin_link


@admin.register(User)
class UserAdmin(UserAdminBase):
    """Admin panel for users."""

    # Form configuration
    fieldsets = (
        (None, {"fields": ("email", "password")}),
        ("Personal Info", {"fields": ("first_name", "last_name")}),
        (
            "Permissions",
            {"fields": ("is_active", "is_partial", "is_staff", "is_superuser")},
        ),
        (
            "Meta",
            {"fields": ("last_login", "verified_at", "upgraded_at", "created_at")},
        ),
    )
    add_fieldsets = (
        (None, {"classes": ("wide",), "fields": ("email", "password1", "password2")}),
        ("Profile", {"fields": ("first_name", "last_name")}),
        ("Permissions", {"fields": ("is_active", "account_type", "is_staff")}),
    )
    readonly_fields = (
        "created_at",
        "upgraded_at",
        "is_partial",
        "last_login",
    )

    # List configuration
    list_display = (
        "email",
        "first_name",
        "last_name",
        "is_verified",
        "is_partial",
        "is_active",
        "created_at",
        "link_profile",
    )
    list_filter = ("is_active",)
    search_fields = ("email", "first_name", "last_name")
    ordering = ("email",)

    def get_queryset(self, request):
        """Override list query to improve query performance (associations)."""
        return (
            super(UserAdminBase, self).get_queryset(request).select_related("profile")
        )

    def link_profile(self, obj):
        """Link to associated user."""
        return get_associated_admin_link(obj, "profile", "user_profile", None)

    link_profile.short_description = "Profile"


@admin.register(UserEmailCode)
class EmailCodeAdmin(admin.ModelAdmin):
    """Admin interface for email codes of various types."""

    fieldsets = (
        (None, {"fields": ("code", "type", "is_valid", "user")}),
        ("Dates", {"fields": ("expires_at", "used_at", "invalidated_at")}),
        (None, {"fields": ("created_at", "updated_at")}),
    )
    list_display = (
        "id",
        "code",
        "type",
        "is_valid",
        "expires_at",
        "used_at",
        "invalidated_at",
        "link_user",
        "created_at",
    )
    list_filter = ("type",)
    list_per_page = 100
    readonly_fields = ("is_valid", "created_at", "updated_at")

    def link_user(self, obj):
        """Link to the generic object associated with the email code."""
        return get_associated_admin_link(obj, "user", "user_user", None)

    link_user.short_description = "User"


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    """Admin panel for user profiles."""

    # Form configuration
    fieldsets = (
        (
            None,
            {"fields": ("get_user_name", "gender", "date_of_birth", "phone_number")},
        ),
    )
    readonly_fields = ("get_user_name",)

    # List configuration
    list_display = (
        "get_user_name",
        "gender",
        "date_of_birth",
        "phone_number",
        "link_user",
    )
    list_filter = ("gender",)
    search_fields = ("get_user_name",)

    def get_user_name(self, obj):
        """Get associated user's name."""
        return obj.user.full_name

    get_user_name.admin_order_field = "user__last_name"
    get_user_name.short_description = "Name"

    def get_queryset(self, request):
        """Override list query to improve query performance (associations)."""
        return super(ProfileAdmin, self).get_queryset(request).select_related("user")

    def link_user(self, obj):
        """Link to associated user."""
        return get_associated_admin_link(obj, "user", "user_user")

    link_user.short_description = "User"

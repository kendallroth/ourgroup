"""User module configuration."""

from django.apps import AppConfig


class UserConfig(AppConfig):
    """User module configuration."""

    name = "user"

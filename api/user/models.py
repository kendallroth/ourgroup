"""User models."""

from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone

from util.errors import Errors
from util.misc import phone_regex
from util.models import BaseModel, BaseEmailCode


class UserManager(BaseUserManager):
    """Django manager for user model."""

    use_in_migrations = True

    def create_user_base(self, email, password, **extra_fields):
        """Shared method of creating new users."""
        if not email:
            raise ValueError("Email is required for new users")

        # Users must have a profile created
        profile = Profile()
        profile.save()

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.profile = profile
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create new regular user."""
        extra_fields.setdefault("is_staff", False)
        extra_fields.setdefault("is_superuser", False)
        return self.create_user_base(email, password, **extra_fields)

    def create_superuser(self, email, password=None, **extra_fields):
        """Create new superuser."""
        extra_fields.setdefault("is_staff", True)
        extra_fields.setdefault("is_superuser", True)
        extra_fields.setdefault("is_active", True)
        extra_fields.setdefault("verified_at", timezone.now())

        if extra_fields.get("is_staff") is not True:
            raise ValueError("Superuser must have is_staff=True")
        if extra_fields.get("is_superuser") is not True:
            raise ValueError("Superuser must have is_superuser=True")

        return self.create_user_base(email, password, **extra_fields)


class Profile(BaseModel):
    """User profile."""

    # Utilities
    GENDER_MALE = "MALE"
    GENDER_FEMALE = "FEMALE"
    GENDER_OTHER = "OTHER"
    GENDER_CHOICES = (
        (GENDER_MALE, "Male"),
        (GENDER_FEMALE, "Female"),
        (GENDER_OTHER, "Other"),
    )

    # Fields
    gender = models.CharField(
        help_text="Gender", choices=GENDER_CHOICES, blank=True, max_length=25, null=True
    )
    date_of_birth = models.DateField(help_text="Birth Date", blank=True, null=True)
    phone_number = models.CharField(
        help_text="Phone Number",
        blank=True,
        null=True,
        max_length=17,
        validators=[phone_regex],
    )


# Implementation for custom user class:
#   https://www.fomfus.com/articles/how-to-use-email-as-username-for-django-authentication-removing-the-username
class User(AbstractUser, BaseModel):
    """User model."""

    # NOTE: Model replaces username with email as default user identifier

    # Utilities
    ACCOUNT_TYPE_CREATED = "CREATED"
    ACCOUNT_TYPE_INVITED = "INVITED"
    ACCOUNT_TYPE_CHOICES = (
        (ACCOUNT_TYPE_CREATED, "Created"),
        (ACCOUNT_TYPE_INVITED, "Invited"),
    )

    # Fields
    username = None
    email = models.EmailField(help_text="Email Address", max_length=128, unique=True)
    password = models.CharField(help_text="Password", max_length=128)
    first_name = models.CharField(help_text="First Name", max_length=50)
    last_name = models.CharField(help_text="Last Name", max_length=50)
    is_active = models.BooleanField(help_text="Active", default=False)
    is_staff = models.BooleanField(help_text="Is Staff", default=False)
    is_superuser = models.BooleanField(help_text="Is Superuser", default=False)
    account_type = models.CharField(
        help_text="Account Type",
        choices=ACCOUNT_TYPE_CHOICES,
        default=ACCOUNT_TYPE_CREATED,
        max_length=25,
    )
    last_login = models.DateTimeField(help_text="Last Login", default=None, null=True)
    verified_at = models.DateTimeField(
        help_text="Date Verified", blank=True, default=None, null=True
    )
    upgraded_at = models.DateTimeField(
        help_text="Date Upgraded", default=None, null=True
    )

    # Relationships
    profile = models.OneToOneField(
        Profile,
        help_text="User profile",
        # Should warn when trying to delete a user's profile
        on_delete=models.PROTECT,
    )

    USERNAME_FIELD = "email"
    # Email field will be automatically included as the new "USERNAME_FIELD"
    REQUIRED_FIELDS = []

    objects = UserManager()

    # Calculated fields
    @property
    def full_name(self):
        """User's full name."""
        return "%s %s" % (self.first_name, self.last_name)

    def is_partial(self):
        """Whether user account is only partial."""
        return (
            self.account_type == self.ACCOUNT_TYPE_INVITED and self.upgraded_at is None
        )

    is_partial.boolean = True

    def is_verified(self):
        """Whether user is verified."""
        return self.verified_at is not None

    is_verified.boolean = True

    # Methods
    def set_password(self, password=None):
        """Run test against password strength library before saving.

        Args:
            password (str, optional): New password

        Raises:
            ValidationError: If password fails validation or is same as the previous
                password
        """
        if password is None:
            raise ValidationError(Errors.PASSWORD_WEAK)

        # Cannot change password to original value
        if super().check_password(password):
            raise ValidationError(Errors.PASSWORD_SAME)

        # TODO: Check password strength with zxcvbn
        # NOTE: Throws "ValidationException" if password validation fails
        try:
            validate_password(password)
        except Exception:
            raise ValidationError(Errors.PASSWORD_WEAK)

        super().set_password(password)


class UserEmailCode(BaseEmailCode):
    """User email code."""

    # Utilities
    ACCOUNT_INVITATION = "ACCOUNT_INVITATION"
    ACCOUNT_UPGRADE = "ACCOUNT_UPGRADE"
    ACCOUNT_VERIFICATION = "ACCOUNT_VERIFICATION"
    EMAIL_VERIFICATION = "EMAIL_VERIFICATION"
    PASSWORD_RESET = "PASSWORD_RESET"
    EMAIL_CODE_CHOICES = (
        (ACCOUNT_INVITATION, "Account Invitation"),
        (ACCOUNT_UPGRADE, "Account Upgrade"),
        (ACCOUNT_VERIFICATION, "Account Verification"),
        (EMAIL_VERIFICATION, "Email Verification"),
        (PASSWORD_RESET, "Password Reset"),
    )

    # Fields
    type = models.CharField(
        choices=EMAIL_CODE_CHOICES,
        help_text="Type of email code (redundant)",
        max_length=125,
    )

    # Relationships
    user = models.ForeignKey(
        User,
        help_text="User email code",
        # Should warn when trying to delete a user's email code
        on_delete=models.PROTECT,
    )

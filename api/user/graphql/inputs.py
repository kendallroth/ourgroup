"""GraphQL mutation inputs for users module."""

import graphene

from user.graphql.types import GenderEnum


class UserActivateInput(graphene.InputObjectType):
    """User activation input type."""

    code = graphene.String(
        description="Code from account activation email", required=True
    )


class UserActivationResendInput(graphene.InputObjectType):
    """User activation resend input type."""

    email = graphene.String(description="User email", required=True)


class UserSignUpInput(graphene.InputObjectType):
    """User signup input type."""

    email = graphene.String(description="User email", required=True)
    name = graphene.String(description="User name", required=True)
    password = graphene.String(description="User password", required=True)


class UserPasswordChangeInput(graphene.InputObjectType):
    """User password change input type."""

    password = graphene.String(description="Current account password", required=True)
    password_new = graphene.String(description="New account password", required=True)


class UserPasswordForgetInput(graphene.InputObjectType):
    """User password forget input type."""

    email = graphene.String(required=True)


class UserPasswordResetInput(graphene.InputObjectType):
    """User password reset input type."""

    code = graphene.String(
        description="Code from password reset request email", required=True
    )
    password = graphene.String(description="New account password", required=True)


class UserProfileUpdateInput(graphene.InputObjectType):
    """User profile update input type."""

    date_of_birth = graphene.Date(description="User date of birth")
    gender = graphene.Field(GenderEnum, description="User gender")
    first_name = graphene.String(description="User first name")
    last_name = graphene.String(description="User last name")
    phone_number = graphene.String(description="User phone number")

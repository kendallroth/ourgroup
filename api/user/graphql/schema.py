"""GraphQL schema for user module."""

import graphene
import graphql_jwt

from user.graphql.mutations import (
    UserActivate,
    UserActivationResend,
    UserSignUp,
    UserPasswordChange,
    UserPasswordForget,
    UserPasswordReset,
    UserProfileUpdate,
)
from user.graphql.types import (
    UserEmailCode as UserEmailCodeType,
    UserEmailCodeEnum,
    UserNode,
)
from user.models import UserEmailCode
from util.auth import require_login
from util.misc import catch_uuid


class Query(graphene.ObjectType):
    """User schema queries."""

    email_code = graphene.Field(
        UserEmailCodeType,
        code=graphene.String(required=True),
        codeType=graphene.Argument(UserEmailCodeEnum),
    )
    viewer = graphene.Field(UserNode)

    @catch_uuid
    def resolve_email_code(self, info, **kwargs):
        """Resolve a user email code."""
        return UserEmailCode.objects.filter(
            code=kwargs["code"], type=kwargs["codeType"]
        ).first()

    @require_login
    def resolve_viewer(self, info):
        """Get the current viewer from the request context (JWT)."""
        return info.context.user


class Mutation(graphene.ObjectType):
    """User schema mutations."""

    # Authentication
    auth_token = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    delete_token_cookie = graphql_jwt.DeleteJSONWebTokenCookie.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    # Long running refresh tokens
    revoke_token = graphql_jwt.Revoke.Field()
    delete_refresh_token_cookie = graphql_jwt.DeleteRefreshTokenCookie.Field()

    # User
    user_activate = UserActivate.Field()
    user_activation_resend = UserActivationResend.Field()
    user_sign_up = UserSignUp.Field()
    user_profile_update = UserProfileUpdate.Field()
    user_password_change = UserPasswordChange.Field()
    user_password_forget = UserPasswordForget.Field()
    user_password_reset = UserPasswordReset.Field()

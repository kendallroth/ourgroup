"""GraphQL types for user module."""

import graphene

from graphene import relay
from graphene_django import DjangoObjectType

from user.models import Profile, User, UserEmailCode as UserEmailCodeModel
from util.graphql.types import EmailCode as EmailCodeType


class UserProfile(DjangoObjectType):
    """User profile."""

    email = graphene.String()
    name = graphene.String()

    class Meta:
        """Meta class for UserProfile."""

        model = Profile

    def resolve_email(self, info, **kwargs):
        """Email resolver."""
        return self.user.email

    def resolve_name(self, info, **kwargs):
        """Name resolver."""
        return self.user.full_name


class UserNode(DjangoObjectType):
    """User type."""

    name = graphene.String(description="User name (full)")
    profile = graphene.Field(UserProfile)

    class Meta:
        """Meta class for UserNode."""

        model = User
        exclude_fields = ("password",)
        interfaces = (relay.Node,)

    def resolve_name(self, info, **kwargs):
        """Name resolver."""
        return self.full_name

    def resolve_profile(self, info, **kwargs):
        """Profile resolver."""
        return self.profile


class UserEmailCode(EmailCodeType):
    """User email code."""

    user = graphene.Field(UserNode)

    class Meta:
        """Meta class for UserEmailCode."""

        model = UserEmailCodeModel

    def resolve_user(self, info, **kwargs):
        """Email code user resolver."""
        return self.user


# Hacky way of getting access to the generated enum from Django choices
# Taken from: https://github.com/graphql-python/graphene-django/issues/570
GenderEnum = UserProfile._meta.fields["gender"].type
UserEmailCodeEnum = UserEmailCode._meta.fields["type"].type

"""GraphQL mutations for users module."""

import graphene

from datetime import timedelta
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.db import transaction
from django.db.models import Q
from django.utils import timezone
from graphql import GraphQLError

from user.graphql.types import UserNode
from user.graphql.inputs import (
    UserActivateInput,
    UserActivationResendInput,
    UserPasswordChangeInput,
    UserPasswordForgetInput,
    UserPasswordResetInput,
    UserProfileUpdateInput,
    UserSignUpInput,
)
from user.models import UserEmailCode, Profile, User
from util.auth import require_login
from util.errors import Errors, GraphQLErrors, handle_validations
from util.misc import catch_uuid


class UserActivate(graphene.Mutation):
    """Activate a user."""

    ok = graphene.Boolean()
    user = graphene.Field(UserNode)

    class Arguments:
        """UserActivate arguments."""

        input = UserActivateInput(required=True)

    @catch_uuid
    @transaction.atomic()
    def mutate(self, info, input):
        """Mutation to activate a user."""
        code = UserEmailCode.objects.filter(
            code=input.code, type=UserEmailCode.ACCOUNT_VERIFICATION
        ).first()

        if code is None:
            raise GraphQLError(GraphQLErrors.USER_ACTIVATE__CODE_INVALID)

        # Codes can be invalid in multiple ways
        if timezone.now() > code.expires_at:
            raise GraphQLError(GraphQLErrors.USER_ACTIVATE__CODE_EXPIRED)
        elif code.invalidated_at is not None:
            raise GraphQLError(GraphQLErrors.USER_ACTIVATE__CODE_INVALIDATED)
        elif code.used_at is not None:
            raise GraphQLError(GraphQLErrors.USER_ACTIVATE__CODE_USED_ALREADY)

        # Flag the used code
        code.used_at = timezone.now()
        code.save()

        # Activate the user
        actor = code.user
        actor.is_active = True

        # Only set the verified date the first time a user is verified
        if actor.verified_at is None:
            actor.verified_at = timezone.now()

        actor.save()

        return UserActivate(ok=True, user=actor)


class UserActivationResend(graphene.Mutation):
    """Resend a user activation email."""

    ok = graphene.Boolean()

    class Arguments:
        """UserActivationResend arguments."""

        input = UserActivationResendInput(required=True)

    @transaction.atomic()
    def mutate(self, info, input):
        """Mutation to resend a user activation email."""
        actor = User.objects.filter(email__iexact=input.email).first()

        # All requests should succeed to avoid revealing email addresses
        if actor is None:
            return UserActivationResend(True)

        # Avoid sending emails to already verified/activated users
        # NOTE: This is different than the "is_active" toggle, which is solely
        #         reserved as a "deleted" flag.
        if actor.verified_at is not None:
            raise GraphQLError(GraphQLErrors.USER_ACTIVATION_RESEND__ALREADY_ACTIVATED)

        # Invalidate previous "active" account activation codes
        UserEmailCode.objects.filter(
            user=actor,
            invalidated_at=None,
            type=UserEmailCode.ACCOUNT_VERIFICATION,
            used_at=None,
        ).update(invalidated_at=timezone.now())

        # Account activation requests expire in a day
        new_activation_expiry = timezone.now() + timedelta(days=1)
        new_activation = UserEmailCode(
            expires_at=new_activation_expiry,
            type=UserEmailCode.ACCOUNT_VERIFICATION,
            user=actor,
        )
        new_activation.save()

        print(new_activation.code)

        # TODO: Audit log

        # TODO: Create email

        return UserActivationResend(True)


class UserPasswordChange(graphene.Mutation):
    """Change a user's password."""

    user = graphene.Field(UserNode)

    class Arguments:
        """UserPasswordChange arguments."""

        input = UserPasswordChangeInput(required=True)

    @require_login
    @transaction.atomic()
    def mutate(self, info, input):
        """Mutation to change a password."""
        actor = info.context.user

        validations = []

        if not actor.check_password(input.password):
            validations.append(GraphQLErrors.USER_PASSWORD_CHANGE__INVALID_CREDENTIALS)

        # Users cannot use the same password twice in a row
        if actor.check_password(input.password_new):
            validations.append(GraphQLErrors.USER_PASSWORD_CHANGE__PASSWORD_SAME)

        # Validate password strength (after ensuring code is valid)
        # NOTE: Technically duplicated below but nice for client validation
        try:
            validate_password(input.password_new)
        except ValidationError:
            validations.append(GraphQLErrors.USER_PASSWORD_CHANGE__PASSWORD_WEAK)

        # All validation errors should be grouped together in one error
        handle_validations(validations)

        # Change the user's password
        try:
            actor.set_password(input.password_new)
            actor.save()
        except ValidationError as e:
            if e.message == Errors.PASSWORD_SAME:
                raise GraphQLError(GraphQLErrors.USER_PASSWORD_CHANGE__PASSWORD_SAME)
            elif e.message == Errors.PASSWORD_WEAK:
                raise GraphQLError(GraphQLErrors.USER_PASSWORD_CHANGE__PASSWORD_WEAK)
            else:
                raise GraphQLError(GraphQLErrors.USER_PASSWORD_CHANGE__PASSWORD_INVALID)

        return UserPasswordReset(actor)


class UserPasswordForget(graphene.Mutation):
    """Request a password reset email."""

    ok = graphene.Boolean()

    class Arguments:
        """UserPasswordForget arguments."""

        input = UserPasswordForgetInput(required=True)

    @transaction.atomic()
    def mutate(self, info, input):
        """Mutation to request a password reset email."""
        actor = User.objects.filter(email__iexact=input.email).first()

        # All requests should succeed to avoid revealing email addresses
        if actor is None:
            return UserPasswordForget(True)

        # Invalidate previous "active" password reset codes
        UserEmailCode.objects.filter(
            user=actor,
            invalidated_at=None,
            type=UserEmailCode.PASSWORD_RESET,
            used_at=None,
        ).update(invalidated_at=timezone.now())

        # Password reset requests expire in a day
        new_request_expiry = timezone.now() + timedelta(days=1)
        new_request = UserEmailCode(
            expires_at=new_request_expiry,
            type=UserEmailCode.PASSWORD_RESET,
            user=actor,
        )
        new_request.save()

        print(new_request.code)

        # TODO: Audit log

        # TODO: Create email

        return UserPasswordForget(True)


class UserPasswordReset(graphene.Mutation):
    """Reset a user's password."""

    user = graphene.Field(UserNode)

    class Arguments:
        """UserPasswordReset arguments."""

        input = UserPasswordResetInput(required=True)

    @catch_uuid
    @transaction.atomic()
    def mutate(self, info, input):
        """Mutation to reset a password."""
        code = UserEmailCode.objects.filter(
            code=input.code, type=UserEmailCode.PASSWORD_RESET
        ).first()

        if code is None:
            raise GraphQLError(GraphQLErrors.USER_PASSWORD_RESET__CODE_INVALID)

        # Codes can be invalid in multiple ways
        if timezone.now() > code.expires_at:
            raise GraphQLError(GraphQLErrors.USER_PASSWORD_RESET__CODE_EXPIRED)
        elif code.invalidated_at is not None:
            raise GraphQLError(GraphQLErrors.USER_PASSWORD_RESET__CODE_INVALIDATED)
        elif code.used_at is not None:
            raise GraphQLError(GraphQLErrors.USER_PASSWORD_RESET__CODE_USED_ALREADY)

        validations = []

        # Validate password strength (after ensuring code is valid)
        # NOTE: Technically duplicated below but nice for client validation
        try:
            validate_password(input.password)
        except ValidationError:
            validations.append(GraphQLErrors.USER_PASSWORD_RESET__PASSWORD_WEAK)

        actor = code.user

        # Users cannot use the same password twice in a row
        if actor.check_password(input.password):
            validations.append(GraphQLErrors.USER_PASSWORD_RESET__PASSWORD_SAME)

        # All validation errors should be grouped together in one error
        handle_validations(validations)

        # Flag the used code
        code.used_at = timezone.now()
        code.save()

        # Update the user's password
        try:
            actor.set_password(input.password)
            actor.save()
        except ValidationError as e:
            if e.message == Errors.PASSWORD_SAME:
                raise GraphQLError(GraphQLErrors.USER_PASSWORD_RESET__PASSWORD_SAME)
            elif e.message == Errors.PASSWORD_WEAK:
                raise GraphQLError(GraphQLErrors.USER_PASSWORD_RESET__PASSWORD_WEAK)
            else:
                raise GraphQLError(GraphQLErrors.USER_PASSWORD_RESET__PASSWORD_INVALID)

        return UserPasswordReset(actor)


class UserProfileUpdate(graphene.Mutation):
    """Update a user profile."""

    user = graphene.Field(UserNode)

    class Arguments:
        """UserProfileUpdate arguments."""

        input = UserProfileUpdateInput(required=True)

    @transaction.atomic
    @require_login
    def mutate(self, info, input):
        """Mutation to update a user profile."""
        actor = info.context.user
        profile = actor.profile

        # NOTE: All users should have profiles already...
        if not profile:
            profile = Profile.objects.create(**input)
            actor.profile = profile
            actor.save()

        for attr, value in dict(input).items():
            if type(value) == "str":
                setattr(profile, attr, value.strip())
            else:
                setattr(profile, attr, value)
        profile.save()

        actor.first_name = input["first_name"].strip()
        actor.last_name = input["last_name"].strip()

        actor.save()

        return UserProfileUpdate(user=actor)


class UserSignUp(graphene.Mutation):
    """Create a new user."""

    user = graphene.Field(UserNode)

    class Arguments:
        """UserSignup arguments."""

        input = UserSignUpInput(required=True)

    @transaction.atomic
    def mutate(self, info, input):
        """Mutation to create a new user."""
        validations = []

        # Email must be unique
        existing_user = User.objects.filter(Q(email__iexact=input.email)).first()
        if existing_user is not None:
            validations.append(GraphQLErrors.USER_SIGNUP__DUPLICATE_ACCOUNT)

        # Validate name (and split into parts)
        name_parts = input.name.split(" ", 1)
        first_name = name_parts[0].strip()
        last_name = name_parts[1].strip()

        if len(first_name) < 2 or len(last_name) < 2:
            validations.append(GraphQLErrors.USER_SIGNUP__INVALID_NAME)

        # NOTE: Technically duplicated below but nice for client validation
        try:
            validate_password(input.password)
        except ValidationError:
            validations.append(GraphQLErrors.USER_SIGNUP__PASSWORD_WEAK)

        # All validation errors should be grouped together in one error
        handle_validations(validations)

        actor = User(email=input.email, first_name=first_name, last_name=last_name)
        actor.profile = Profile.objects.create()

        try:
            actor.set_password(input.password)
        except ValidationError as e:
            if e.message == Errors.PASSWORD_WEAK:
                raise GraphQLError(GraphQLErrors.USER_SIGNUP__PASSWORD_WEAK)
            else:
                raise GraphQLError(GraphQLErrors.USER_SIGNUP__PASSWORD_INVALID)

        actor.save()

        # Account activation requests expire in a day
        new_activation_expiry = timezone.now() + timedelta(days=1)
        new_activation = UserEmailCode(
            expires_at=new_activation_expiry,
            type=UserEmailCode.ACCOUNT_VERIFICATION,
            user=actor,
        )
        new_activation.save()

        print(new_activation.code)

        # TODO: Audit log

        # TODO: Send email

        return UserSignUp(user=actor)

# Authentication

## JWT Workflow

Authentication is managed by the Django platform, utilizing [JWT](https://jwt.io/) tokens to represent a user. The JWT tokens are securely stored in browser cookies to prevent [common JWT security pitfalls](https://hasura.io/blog/best-practices-of-using-jwt-with-graphql/) (such as storing in `localStorage`, etc). Since these tokens represent a user, they expire within a few minutes, necessitating a secure token refresh workflow (using [`django-graphql-jwt`](https://django-graphql-jwt.domake.io/en/latest/authentication.html#)).

As specified in [this tutorial by Hasura](https://hasura.io/blog/best-practices-of-usin), Both a JWT and refresh token are created upon authentication, and added to the response cookies. Future requests from the app will use this JWT cookie to authenticate with the API. When a request is made with an expired JWT, a refresh request will automatically be made. If successful, the original request will be repeated with the new JWT token. This ensures that the user does not need to repeatedly sign in. Upon logout, two mutations are used to remove the JWT and refresh token cookies (essentially unauthenticating the user).

_Ideally the app would automatically refresh the JWT when the previous token is near expiry. A refresh query would immediately be sent when opening the app, which would provide a new JWT and its expiry. Once this expiry was near, another refresh token would be requested, and the cycle would repeat._

> Due to Django Admin's use of session cookies, using the app while a Django Admin session is open will result in the app verifying as the Django Admin user!

On page load, the app uses the initial `Viewer` query (in the `App` component) to determine whether the user is authenticated. No other data requiring validation should be requested in the `App` component! Once the `Viewer` query has completed, the current route will be checked to ensure it is accessible with the user's authentication status. Authentication errors will redirect to the sign in page (with a redirect URL). A successful query will toggle the app's authentication flag and allow the rest of the app to render (loading their own queries as components mount).

## Authentication Backend

A custom backend was created to enable authentication via email (rather than username). This backend also throws authentication errors rather than simply returning `None` (which doesn't indicate what was wrong). Since users can be inactive or unverified, conveying this to the API consumer can help indicate necessary corrections.

> However, throwing errors when authenticating means the Django admin panel doesn't handle login errors...at all...

